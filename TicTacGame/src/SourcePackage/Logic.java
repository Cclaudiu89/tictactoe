package SourcePackage;

public class Logic {

	private char[][] board;
	private char currentPlayerMark;
	
	public Logic() {
		board = new char[3][3];
        currentPlayerMark = 'x';
        initializeBoard();
	}
	
	 public char getCurrentPlayerMark()
	 {
	        return currentPlayerMark;
	 }
	
	public void initializeBoard() {
		
		for(int i = 0; i < 3; i++) {
			
			for(int j = 0; j < 3; j++) {
				board[i][j] = '-';
			}
			
		}
		
	}
	
	public void printBoard() {
		System.out.println();

        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " | ");
            }
            System.out.println();
            if(i != 2) {
            System.out.println("-------------");
            }else if(i == 2) {
            	System.out.println();
            }
        }
    }
	
	public boolean isBoardFull() {
		boolean isFull = true;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    isFull = false;
                }
            }
        }

        return isFull;
	}
	
	public boolean checkForWin() {
		
		return (checkRowsForWin() || checkColumnsForWin() || checkDiagonalsForWin());
	}
	
	private boolean checkRowsForWin() {
		for (int i = 0; i < 3; i++) {
            if (checkRowsCol(board[i][0], board[i][1], board[i][2]) == true) {
                return true;
            }
        }
        return false;
	}
	
	private boolean checkColumnsForWin() {
		for (int i = 0; i < 3; i++) {
            if (checkRowsCol(board[0][i], board[1][i], board[2][i]) == true) {
                return true;
            }
        }
        return false;
	}
	
	private boolean checkDiagonalsForWin() {
		
		return ((checkRowsCol(board[0][0], board[1][1], board[2][2]) == true) || (checkRowsCol(board[0][2], board[1][1], board[2][0]) == true));
	}
	
	private boolean checkRowsCol(char c1, char c2, char c3) {
		
		return ((c1 != '-') && (c1 == c2) && (c2 == c3));
	}
	
	public void changePlayer() {
        if (currentPlayerMark == 'x') {
            currentPlayerMark = 'o';
        }
        else {
            currentPlayerMark = 'x';
        }
    }
	
	public boolean placeMark(int row, int col) {

        // Make sure that row and column are in bounds of the board.
        if ((row >= 0) && (row < 3)) {
            if ((col >= 0) && (col < 3)) {
                if (board[row][col] == '-') {
                    board[row][col] = currentPlayerMark;
                    return true;
                }
            }
        }

        return false;
    }
	
}
